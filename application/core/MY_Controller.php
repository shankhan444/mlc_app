<?php defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->library('session');
    }
    function page_construct($page, $metaData = array(), $data = array())
    {
        if ($this->session->userdata('isLogged') === TRUE)
        {
            if($page=="login/login")
            {

                $data['errorData'] = "";
                $this->load->view($page, $data);
                $this->load->view('theme/footer');
            }
            else
            {
                $this->load->view('theme/header' ,$metaData);
                $this->load->view($page, $data);
                $this->load->view('theme/footer');
            }
        }
        else
        {
            $this->load->view($page, $data);
            $this->load->view('theme/footer');
        }
    }

}
