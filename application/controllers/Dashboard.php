<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Dashboard extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Attendence_model');

    }

    public function index()
    {
        if ($this->session->userdata('isLogged') === TRUE)
        {
            $metaData['pageTitle']="Dashboard";
            $attendance_data           =  array();
            $attendance_data['result'] =  $this->Attendence_model->get();
            $attendance_data['users'] =  $this->Attendence_model->getAllUsers();
//            print_r($jobs_data);
            $this->page_construct('dashboard/index',$metaData, $attendance_data);
        }
        else
        {
            $metaData['pageTitle'] = "Login";
            $data['pageTitle'] = "Login";
            $this->page_construct('login/login', $data, $data);
        }
    }
    public function logout()
    {
        $this->session->unset_userdata($_SESSION);
        $this->session->sess_destroy();
        redirect("login", "refresh");
    }





}
?>
