<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Attendance extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Attendence_model');
    }

    public function getAreasByUserId()
    {
        $users =$this->Attendence_model->getAreasByUserId($_GET['userId']);
        echo json_encode($users);
    }
    public function getAttendanceByUserIdAndAreaId()
    {
        $attendance =$this->Attendence_model->getAttendanceByUserIdAndAreaId($_GET['userId'],$_GET['areaId']);
        echo json_encode($attendance);
    }
}
?>
