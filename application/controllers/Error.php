<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Error extends MY_Controller
{

    function __construct()
    {
        parent::__construct();

    }

    public function index()
    {
        $metaData['pageTitle']='404';
        $this->load->view('errors/error404',$metaData);
        $this->load->view('theme/footer');
    }
}
?>
