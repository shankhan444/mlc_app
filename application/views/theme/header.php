<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title> <?php echo $pageTitle; ?> </title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <link href="<?php echo base_url('assets/css/vendor.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/app.css');?>" rel="stylesheet">
</head>

<body>
<div class="main-wrapper">
    <div class="app" id="app">
        <header class="header">
            <div class="header-block header-block-collapse hidden-lg-up">
                <button class="collapse-btn" id="sidebar-collapse-btn">
                    <i class="fa fa-bars"></i>
                </button> </div>
            <div class="header-block header-block-search hidden-sm-down">
                <form role="search">
                    <div class="input-container"> <i class="fa fa-search"></i>
                        <input type="search" placeholder="Search">
                        <div class="underline"></div>
                    </div>
                </form>
            </div>
            <div class="header-block header-block-nav">
                <ul class="nav-profile">
                    <li class="notifications new"> <a href="" data-toggle="dropdown">
                            <i class="fa fa-bell-o"></i>
                            <sup>
                                <span class="counter">8</span>
                            </sup>
                        </a>
                        <div class="dropdown-menu notifications-dropdown-menu">
                            <ul class="notifications-container">
                                <li>
                                    <a href="" class="notification-item">
                                        <div class="img-col">
                                            <div class="img" style="background-image: url('assets/faces/3.jpg')"></div>
                                        </div>
                                        <div class="body-col">
                                            <p> <span class="accent">Zack Alien</span> pushed new commit: <span class="accent">Fix page load performance issue</span>. </p>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="" class="notification-item">
                                        <div class="img-col">
                                            <div class="img" style="background-image: url('assets/faces/5.jpg')"></div>
                                        </div>
                                        <div class="body-col">
                                            <p> <span class="accent">Amaya Hatsumi</span> started new task: <span class="accent">Dashboard UI design.</span>. </p>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="" class="notification-item">
                                        <div class="img-col">
                                            <div class="img" style="background-image: url('assets/faces/8.jpg')"></div>
                                        </div>
                                        <div class="body-col">
                                            <p> <span class="accent">Andy Nouman</span> deployed new version of <span class="accent">NodeJS REST Api V3</span> </p>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                            <footer>
                                <ul>
                                    <li> <a href="">
                                            View All
                                        </a> </li>
                                </ul>
                            </footer>
                        </div>
                    </li>
                    <li class="profile dropdown">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                            <div class="img" style="background-image: url('https://avatars3.githubusercontent.com/u/3959008?v=3&s=40')"> </div> <span class="name">
    			      MLC Admin
    			    </span> </a>
                        <div class="dropdown-menu profile-dropdown-menu" aria-labelledby="dropdownMenu1"> <a class="dropdown-item" href="#">
                                <i class="fa fa-user icon"></i>
                                Profile
                            </a> <a class="dropdown-item" href="#">
                                <i class="fa fa-bell icon"></i>
                                Notifications
                            </a> <a class="dropdown-item" href="#">
                                <i class="fa fa-gear icon"></i>
                                Settings
                            </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="<?php echo base_url('dashboard/logout')?>">
                                <i class="fa fa-power-off icon"></i>
                                Logout
                            </a> </div>
                    </li>
                </ul>
            </div>
        </header>
        <aside class="sidebar">
            <div class="sidebar-container">
                <div class="sidebar-header">
                    <div class="brand">
                        <div class="logo">
                            <span class="l l1"></span>
                            <span class="l l2"></span>
                            <span class="l l3"></span>
                            <span class="l l4"></span>
                            <span class="l l5"></span>
                        </div><?php echo '     '.appName; ?>  </div>
                </div>
                <nav class="menu">
                    <ul class="nav metismenu" id="sidebar-menu">
                        <li class="active"> <a href="<?php echo base_url('dashboard')?>">
                                <i class="fa fa-home"></i> Dashboard
                            </a>
                        </li>
                        <li>
                            <a href="">
                                <i class="fa fa-user"></i> HR
                                <i class="fa arrow"></i>
                            </a>
                            <ul>
                                <li>
                                    <a href="">
                                        <i class="fa fa-angle-down"></i>
                                       &nbsp;&nbsp;Forms
                                    </a>
                                    <ul>
                                        <li>
                                            <a href="<?php echo 'employee'?>">
                                                <i class="fa fa-user-plus"></i>
                                                &nbsp;Employee Add
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="item-editor.html">
                                        <i class="fa fa-angle-down"></i>
                                        &nbsp;Reports
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="">
                                <i class="fa fa-bar-chart"></i> Charts
                                <i class="fa arrow"></i>
                            </a>
                            <ul>
                                <li> <a href="charts-flot.html">
                                        Flot Charts
                                    </a> </li>
                                <li> <a href="charts-morris.html">
                                        Morris Charts
                                    </a> </li>
                            </ul>
                        </li>
                        <li> <a href="">
                                <i class="fa fa-table"></i> Tables
                                <i class="fa arrow"></i>
                            </a>
                            <ul>
                                <li> <a href="static-tables.html">
                                        Static Tables
                                    </a> </li>
                                <li> <a href="responsive-tables.html">
                                        Responsive Tables
                                    </a> </li>
                            </ul>
                        </li>
                        <li> <a href="forms.html">
                                <i class="fa fa-pencil-square-o"></i> Forms
                            </a> </li>
                        <li> <a href="">
                                <i class="fa fa-desktop"></i> UI Elements
                                <i class="fa arrow"></i>
                            </a>
                            <ul>
                                <li> <a href="buttons.html">
                                        Buttons
                                    </a> </li>
                                <li> <a href="cards.html">
                                        Cards
                                    </a> </li>
                                <li> <a href="typography.html">
                                        Typography
                                    </a> </li>
                                <li> <a href="icons.html">
                                        Icons
                                    </a> </li>
                                <li> <a href="grid.html">
                                        Grid
                                    </a> </li>
                            </ul>
                        </li>
                        <li> <a href="">
                                <i class="fa fa-file-text-o"></i> Pages
                                <i class="fa arrow"></i>
                            </a>
                            <ul>
                                <li> <a href="login.html">
                                        Login
                                    </a> </li>
                                <li> <a href="signup.html">
                                        Sign Up
                                    </a> </li>
                                <li> <a href="reset.html">
                                        Reset
                                    </a> </li>
                                <li> <a href="error-404.html">
                                        Error 404 App
                                    </a> </li>
                                <li> <a href="error-404-alt.html">
                                        Error 404 Global
                                    </a> </li>
                                <li> <a href="error-500.html">
                                        Error 500 App
                                    </a> </li>
                                <li> <a href="error-500-alt.html">
                                        Error 500 Global
                                    </a> </li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
            <footer class="sidebar-footer">

            </footer>
        </aside>
        <div class="sidebar-overlay" id="sidebar-overlay"></div>