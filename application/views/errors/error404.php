
<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title> <?php echo $pageTitle; ?> </title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <link href="<?php echo base_url('assets/css/vendor.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/app.css');?>" rel="stylesheet">
</head>

<body>
<div class="app blank sidebar-opened">
    <article class="content">
        <div class="error-card global">
            <div class="error-title-block">
                <h1 class="error-title">404</h1>
                <h2 class="error-sub-title"> Sorry, page not found </h2>
            </div>
            <div class="error-container">
                <div class="row">
                    <p>We are currently working on it.</p>
                </div>
                <br> <a class="btn btn-primary" href="<?php echo base_url('dashboard')?>">
                    <i class="fa fa-angle-left"></i>
                    Back to Dashboard
                </a> </div>
        </div>
    </article>
</div>
</body>
</html>