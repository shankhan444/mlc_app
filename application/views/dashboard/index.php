<article class="content dashboard-page">
            <section class="section">
                <div class="row sameheight-container">
                    <div class="col col-xs-12 col-sm-12 col-md-12 col-xl-12 stats-col">
                        <form role="form" method="post" action="<?php echo base_url('employee/add') ?>">
                            <div class="row">
                                <div class="form-group col-lg-12">
                                    <label class="control-label" for="supervisorSelectInput">Supervisor</label>
                                    <select class="form-control" id="supervisorSelectInput" name="supervisorId" onchange="populateAreas()">
                                        <option selected disabled>--Select Supervisor--</option>
                                        <?php foreach($users as $user): ?>
                                            <option value="<?php print_r( $user->id); ?>"><?php print_r( $user->firstname." ".$user->lastname); ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="form-group col-lg-6">
                                    <label class="control-label" for="supervisorAreaSelectInput">Area</label>
                                    <select class="form-control" id="supervisorAreaSelectInput" name="supervisorArea" onchange="populateSelectedAreas()">
                                        <option selected Disabled>--Select Supervisor First--</option>
                                    </select>
                                </div>
                            </div>
                        </form>
                        <div class="card sameheight-item stats" data-exclude="xs">
                            <div class="all-areas">
                              <?php foreach($result as $row): ?>
                            <div class="card-block">
                                <div class="row">
                                    <div class="title-block col-sm-8">
                                        <h4 class="title"> Attendance </h4>
                                        <p>
                                            <b> Supervoisor Name
                                            </b>
                                                <?php print_r( $row->firstname); ?>
                                                <?php print_r( $row->lastname); ?>
                                        </p>
                                        <p>
                                            <b>Area Name</b>
                                            <?php print_r( $row->name); ?>
                                        </p>
                                        <p >
                                            <b>Attendance Date And Time</b>
                                            <?php echo date("d-m-Y H:i:a",strtotime( $row->timestamp)); ?>
                                        </p>
                                    </div>
                                    <div class="item-col fixed item-col-img xs col-sm-4">
                                        <a href="http://192.168.100.200/mlc_api/attedance_images/<?php echo $row->image ?>">
                                            <img class="img-thumbnail" src="http://192.168.100.200/mlc_api/attedance_images/<?php echo $row->image ?>" />
                                        </a>
                                    </div>
                                </div>
                                <div class="row row-sm stats-container">
                                    <div class="col-xs-12 col-sm-3 stat-col">
                                        <div class="stat-icon"> <i class="fa fa-users"></i> </div>
                                        <div class="stat">
                                            <div class="value"> <?php print_r( $row->total); ?> </div>
                                            <div class="name"> Total Employee </div>
                                        </div> <progress class="progress stat-progress" value="75" max="<?php print_r( $row->total); ?>">
                                            <div class="progress">
                                                <span class="progress-bar" style="width: 75%;"></span>
                                            </div>
                                        </progress> </div>
                                    <div class="col-xs-12 col-sm-3 stat-col">
                                        <div class="stat-icon"> <i class="fa fa-user"></i> </div>
                                        <div class="stat">
                                            <div class="value"> <?php print_r( $row->present); ?> </div>
                                            <div class="name"> Present </div>
                                        </div> <progress class="progress stat-progress" value="<?php print_r( $row->present); ?>" max="<?php print_r( $row->total); ?>">
                                            <div class="progress">
                                                <span class="progress-bar" style="width: 25%;"></span>
                                            </div>
                                        </progress> </div>
                                    <div class="col-xs-12 col-sm-3  stat-col">
                                        <div class="stat-icon"> <i class="fa fa-user-times"></i> </div>
                                        <div class="stat">
                                            <div class="value"> <?php print_r( $row->absent); ?> </div>
                                            <div class="name"> Absent </div>
                                        </div> <progress class="progress stat-progress" value="<?php print_r( $row->absent); ?>" max="<?php print_r( $row->total); ?>">
                                            <div class="progress">
                                                <span class="progress-bar" style="width: 60%;"></span>
                                            </div>
                                        </progress> </div>
                                    <div class="col-xs-12 col-sm-3  stat-col">
                                        <div class="stat-icon"> <i class="fa fa-user-secret"></i> </div>
                                        <div class="stat">
                                            <div class="value"> <?php print_r( $row->leaves); ?> </div>
                                            <div class="name"> Leaves </div>
                                        </div> <progress class="progress stat-progress" value="<?php print_r( $row->leaves); ?>" max="<?php print_r( $row->total); ?>">
                                            <div class="progress">
                                                <span class="progress-bar" style="width: 34%;"></span>
                                            </div>
                                        </progress> </div>
                                </div>
                            </div>
                            <?php endforeach; ?>
                            </div>
                            <div class="all-areas-new" hidden>

                            </div>
                            <div class="new-areas">

                            </div>
                        </div>
                    </div>
                </div>
            </section>
     </article>
        <footer class="footer">
        </footer>
    </div>
</div>
<!-- Reference block for JS -->
<div class="ref" id="ref">
    <div class="color-primary"></div>
    <div class="chart">
        <div class="color-primary"></div>
        <div class="color-secondary"></div>
    </div>
</div>



<script>
    function populateAreas()
    {
        var data = {userId:$("#supervisorSelectInput").val()};
                var url = '<?php echo base_url("Attendance/getAreasByUserId"); ?>';
        $.ajax({
            url: url,
            type: "GET",
            data: data,
            beforeSend :function(){
                $('#supervisorAreaSelectInput option:gt(0)').remove();
                $('#supervisorAreaSelectInput').find("option:eq(0)").html("Please wait..");
            },
            success: function (result){
                $('#supervisorAreaSelectInput').html("<option disabled selected>Select Area</option>");
                try {

                    var jsonResult = JSON.parse(result);
                    var option = $('<option />');
                    option.attr('value', "0").text("All Areas");
                    $('#supervisorAreaSelectInput').append(option);
                    $(jsonResult).each(function()
                    {
                        var option = $('<option />');
                        option.attr('value', this.id).text(this.name);
                        $('#supervisorAreaSelectInput').append(option);

                    });
                }catch (exp) {
                    alert(exp.message);
                    return exp.message;
                }
            }
        });

    }
    function populateSelectedAreas()
    {

        $(".all-areas").hide();
        var data = {userId:$("#supervisorSelectInput").val(),areaId:$("#supervisorAreaSelectInput").val()};
                var url = '<?php echo base_url("Attendance/getAttendanceByUserIdAndAreaId"); ?>';
        $.ajax({
            url: url,
            type: "GET",
            data: data,
            beforeSend :function(){

            },
            success: function (result){
                $('.new-areas').html(" ");
                try {
                    var jsonResult = JSON.parse(result);
                    $(jsonResult).each(function()
                    {

                    var datanew=
                        '<div class="title-block col-sm-8"><p><b> Supervoisor Name</b>'+this.firstname+this.lastname+'</p>' +
                        '<p><b>Area Name</b>'+this.name+'</p></div>' +
                        '<div class="card-block"><div class="row"><div class="title-block col-sm-8"><h4 class="title"> Attendance </h4> <p><b>Attendance Date And Time</b>'+this.timestamp+' </p></div>'+
                        '<div class="item-col fixed item-col-img xs col-sm-4"><a href=""><img class="img-thumbnail" src="http://192.168.100.200/mlc_api/attedance_images/'+this.image+'" /></a></div></div>'+
                        '<div class="row row-sm stats-container"><div class="col-xs-12 col-sm-3 stat-col"><div class="stat-icon"> <i class="fa fa-users"></i></div>'+
                        '<div class="stat"><div class="value">'+this.total +'</div>'+
                        '<div class="name"> Total Employee </div>'+
                        '</div> <progress class="progress stat-progress" value="75" max="'+this.total+'">'+
                        '<div class="progress"><span class="progress-bar" style="width: 75%;"></span></div></progress> </div>'+
                        '<div class="col-xs-12 col-sm-3 stat-col"><div class="stat-icon"> <i class="fa fa-user"></i> </div><div class="stat">'+
                        '<div class="value">'+this.present+'</div><div class="name"> Present </div>'+
                        '</div> <progress class="progress stat-progress" value="'+this.present+'" max="'+this.total+'">'+
                        '<div class="progress"><span class="progress-bar" style="width: 25%;"></span></div></progress></div>'+
                        '<div class="col-xs-12 col-sm-3  stat-col"><div class="stat-icon"> <i class="fa fa-user-times"></i> </div><div class="stat">'+
                        '<div class="value">'+ this.absent+'</div><div class="name"> Absent </div></div> <progress class="progress stat-progress" value="'+
                        this.absent+'" max="'+this.total+'"><div class="progress"><span class="progress-bar" style="width: 60%;"></span></div>'+
                        '</progress> </div><div class="col-xs-12 col-sm-3  stat-col"><div class="stat-icon"> <i class="fa fa-user-secret"></i> </div><div class="stat">'+
                        '<div class="value">'+this.leaves+'</div><div class="name"> Leaves </div></div> <progress class="progress stat-progress" value="'
                        +this.leaves+'" max="'+this.total+'"><div class="progress"><span class="progress-bar" style="width: 34%;"></span></div></progress> </div></div></div>';

                          $('.new-areas').append(datanew);
                          $('.new-areas').show();

                    });
                }catch (exp) {
                    alert(exp.message);
                    return exp.message;
                }
            }
        });
    }
</script>

