<?php
/**
 * Created by PhpStorm.
 * User: kiani
 * Date: 8/9/2017
 * Time: 9:38 PM
 */
?>
<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title> <?php echo $pageTitle; ?> </title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <link href="<?php echo base_url('assets/css/vendor.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/app.css');?>" rel="stylesheet">
</head>

<body>
<div class="auth">
    <div class="auth-container">
        <div class="card">
            <header class="auth-header">
                <h1 class="auth-title">
                    <div class="logo">
                        <span class="l l1"></span>
                        <span class="l l2"></span>
                        <span class="l l3"></span>
                        <span class="l l4"></span>
                        <span class="l l5"></span>
                    </div> <?php echo '     '.appName; ?> </h1>
            </header>
            <div class="auth-content">
                <p class="text-xs-center" style="color: red"><?php echo $error; ?></p>
                <form id="login-form" action="<?php echo base_url('login/userLogin')?>" method="POST" novalidate="">
                    <div class="form-group">
                        <label for="username">Username</label>
                        <input type="email" class="form-control underlined" name="username" id="username" placeholder="Your email address" required>
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" class="form-control underlined" name="password" id="password" placeholder="Your password" required>
                    </div>
                    <div class="form-group">
                        <label for="remember">
                            <input class="checkbox" id="remember" type="checkbox">
                            <span>Remember me</span>
                        </label>
<!--                        <a href="reset.html" class="forgot-btn pull-right">Forgot password?</a>-->
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-block btn-primary">Login</button>
                    </div>
<!--                    <div class="form-group">-->
<!--                        <p class="text-muted text-xs-center">Do not have an account?-->
<!--                            <a href="signup.html">Sign Up!</a>-->
<!--                        </p>-->
<!--                    </div>-->
                </form>
            </div>
        </div>
    </div>
</div>