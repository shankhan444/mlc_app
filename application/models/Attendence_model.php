<?php

Class Attendence_model extends CI_Model {


    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    function getAttendanceByUserIdAndAreaId($userId,$areaId)
    {
        $qryString='';
        if($areaId==0)
        {
            $qryString = "SELECT a.* ,b.firstname,b.lastname,c.name  FROM areas_attendance a  
                          INNER JOIN user b ON b.id=a.user_id
                          INNER JOIN areas c on c.id=a.area_id
                          WHERE a.user_id=$userId ORDER BY a.timestamp DESC";
            $qry = $this->db->query($qryString);
            $result = $qry->result();
            return $result;
        }
        else if ($areaId!=0) {
            $qryString = "SELECT a.* ,b.firstname,b.lastname,c.name  FROM areas_attendance a  
                          INNER JOIN user b ON b.id=a.user_id
                          INNER JOIN areas c on c.id=a.area_id
                          WHERE a.area_id=$areaId AND a.user_id=$userId ORDER BY a.timestamp DESC";

            $qry = $this->db->query($qryString);
            $result = $qry->result();
            return $result;
        }
    }
    function getAllUsers()
    {
            $qryString = "SELECT * FROM user" ;
            $qry = $this->db->query($qryString);
            $result = $qry->result();
            return $result;
    }
    function getAreasByUserId($userId)
    {
            $qryString = "SELECT * FROM areas WHERE user_id=$userId" ;
            $qry = $this->db->query($qryString);
            $result = $qry->result();
            return $result;
    }
    function get()
    {
        $qryString = "SELECT a.id,a.total,a.present,a.leaves,a.image,a.absent,a.user_id,a.timestamp,
                                 b.id,b.name,b.wardno,b.name,
                                 c.id AS userId ,c.username,c.designation,c.firstname,c.lastname
                          FROM areas_attendance a
                          INNER JOIN areas b ON a.area_id = b.id
                          INNER JOIN user c ON a.user_id = c.id ORDER BY a.timestamp DESC " ;
        $qry = $this->db->query($qryString);
        $result = $qry->result();
        return $result;
    }
}
?>




